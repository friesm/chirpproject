

#!/bin/bash

# trim Illumina TrueSeq standard adapters and trim costomized WGA4 adaptors

prefix=~/Project/chirp/ueberham
for i in `ls $prefix | grep UU_3` 	 
do	

  for lane in L007
    do
     # Set the variable "barcode": Extracting the barcode sequence out of the SampleSheet file. 
      barcode=`awk -F ',' '{print $5}' "$prefix"/"$i"/SampleSheet.csv | tail -n 1`
     # Set Variables for the Filenames including the "barcode"
      R1=""$i""_$barcode"_"$lane"_R1_001.fastq.gz";
      R2=""$i"_"$barcode"_"$lane"_R2_001.fastq.gz";
     #Set variables for the directories
      Dir1="${prefix}/$i/${lane}/Reads/Trimmed" 
      Dir3="${prefix}/$i/${lane}/Reads/Trimmed2_3'" 
      Dir5="${prefix}/$i/${lane}/Reads/Trimmed2_5'" 
      rd3="2_3'"
      rd5="2_5'"

     #make a new target directory for the Trim-files 
     # mkdir -p "$Dir1" 

     #execute cutadapt for all the trimmed files with TrueSeq adaptors and store it to Trimmed-folder
     # echo "start Cutadapt for TrueSeq adaptors "$i" "$lane" "

     # /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -A GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -o "$Dir1"/trimmed."$R1" -p "$Dir1"/trimmed."$R2" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R1" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R2" > "$Dir1"/logfile1
     
     # echo "Cutadapt for TrueSeq adaptors finished "$i" "$lane" "

     #make a new target directory for the Trimmed2-files
      mkdir -p "$Dir3" "$Dir5" 

    
     #execute cutadapt for all the trimmed files with costomzed WGA4 adaptors and store it to Trimmed2-folder
      echo "start Cutadapt for costomized WGA4 adaptors from 3' "$i" "$lane" "
      /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -a  GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG -A GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT  -o "$Dir3"/trimmed"$rd3"."$R1" -p "$Dir3"/trimmed"$rd3"."$R2" "$Dir1"/trimmed."$R1" "$Dir1"/trimmed."$R2" > "$Dir3"/logfile2
      echo "---First read from 3'---"
      grep "Sequence: GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG;" "$Dir3"/logfile2
      echo "---Second read from 3'---"  
      grep "Sequence: GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT;" "$Dir3"/logfile2
      #echo "Cutadapt for costomized WGA4 adaptors finished "$i" "$lane" "


      echo "start Cutadapt for costomized WGA4 adaptors from 5' "$i" "$lane" "
      /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -g  GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG -G  GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -o "$Dir5"/trimmed"$rd5"."$R1" -p "$Dir5"/trimmed"$rd5"."$R2" "$Dir1"/trimmed."$R1" "$Dir1"/trimmed."$R2" > "$Dir5"/logfile2
      echo "---First read from 5'---"
      grep "Sequence: GATCGGAAGAGCACACGTCTGAACTCCAGTCACNNNNNNATCTCGTATGCCGTCTTCTGCTTG;" "$Dir5"/logfile2
      echo "---Second read from 5'---"  
      grep "Sequence: GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT;" "$Dir5"/logfile2
      #echo "Cutadapt for costomized WGA4 adaptors finished "$i" "$lane" "

   done

done

