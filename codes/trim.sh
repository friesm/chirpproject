

#!/bin/bash

# trim Illumina TrueSeq standard adapters and trim costomized WGA4 adaptors

prefix=~/Project/chirp/ueberham
for i in `ls $prefix | grep UU_3` 	 
 do	

   for lane in L007 L008
    do
     # Set the variable "barcode": Extracting the barcode sequence out of the SampleSheet file. 
      barcode=`awk -F ',' '{print $5}' "$prefix"/"$i"/SampleSheet.csv | tail -n 1`
     # Set Variables for the Filenames including the "barcode"
      R1=""$i""_$barcode"_"$lane"_R1_001.fastq.gz";
      R2=""$i"_"$barcode"_"$lane"_R2_001.fastq.gz";
     #Set variables for the directories
      Dir1="${prefix}/$i/${lane}/Reads/Trimmed" 
      Dir2="${prefix}/$i/${lane}/Reads/Trimmed_2" 
       

     #make a new target directory for the Trim-files 
      mkdir -p "$Dir1" 

     #execute cutadapt for all the trimmed files with TrueSeq adaptors and store it to Trimmed-folder
      echo "start Cutadapt for TrueSeq adapters "$i" "$lane" "

      /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -A GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -o "$Dir1"/trimmed."$R1" -p "$Dir1"/trimmed."$R2" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R1" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R2" > "$Dir1"/logfile
     
      echo "---First Read---"
      grep "Sequence: AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC;" "$Dir1"/logfile
      echo "---Second Read---"
      grep "Sequence: GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT;" "$Dir1"/logfile




     #make a new target directory for the Trimmed2-files
      mkdir -p "$Dir3" "$Dir5" 

    
     #execute cutadapt for all the trimmed files with costomzed WGA4 adaptors and store it to Trimmed_2-folder
      echo "start Cutadapt for costomized WGA4 adapters" "$i" "$lane" 
      /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -a TACACGACGCTCTTCCGATCTNNNNNNTGTGTTGGGTGTGTTTGG -A AGACGTGTGCTCTTCCGATCTNNNNNNTGTGTTGGGTGTGTTTGG -o "$Dir2"/trimmed2."$R1" -p "$Dir2"/trimmed2."$R2" "$Dir1"/trimmed."$R1" "$Dir1"/trimmed."$R2" > "$Dir2"/logfile2
      echo "---First Read---"
      grep "Sequence: TACACGACGCTCTTCCGATCTNNNNNNTGTGTTGGGTGTGTTTGG;" "$Dir2"/logfile2
      echo "---Second Read---"  
      grep "Sequence: AGACGTGTGCTCTTCCGATCTNNNNNNTGTGTTGGGTGTGTTTGG;" "$Dir2"/logfile2
      

   done

done

