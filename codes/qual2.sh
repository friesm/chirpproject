#!/bin/bash

# run fastqc on every trimmed file					
lane=L007

prefix=~/Project/chirp/ueberham

for i in `ls $prefix | grep UU_3 `
	 
do	

  for rd in "_3'" "_5'" 
	  
    do

     # Set the variable "barcode": Extracting the barcode sequence out of the SampleSheet file. 
      barcode=`awk -F ',' '{print $5}'  $prefix/$i/SampleSheet.csv | tail -n 1`
     # Set Variables for the Filenames including the "barcode"
      R1="${prefix}/${i}/${lane}/Reads/Trimmed"$rd"/trimmed"$rd".${i}_${barcode}_${lane}_R1_001.fastq.gz";
      R2="${prefix}/${i}/${lane}/Reads/Trimmed"$rd"/trimmed"$rd".${i}_${barcode}_${lane}_R2_001.fastq.gz";
     
              
     #make a new target directory for the QC-files
     mkdir -p "${prefix}/$i/${lane}/QC/Trimmed"$rd"/" 

     #execute fastqc for all the files 
     /home/mrtnfries/anaconda3/envs/qualityControl/bin/fastqc -o "$prefix"/"$i"/"$lane"/QC/Trimmed"$rd" --noextract "$R1" "$R2"

   done

done
