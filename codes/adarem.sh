

#!/bin/bash

# trim Illumina TrueSeq standard adapters and trim costomized WGA4 adaptors

prefix=~/Project/chirp/ueberham
for i in `ls $prefix | grep UU_3` 	 
do	

  for lane in L007
	 # L008
    do
     # Set the variable "barcode": Extracting the barcode sequence out of the SampleSheet file. 
      barcode=`awk -F ',' '{print $5}' "$prefix"/"$i"/SampleSheet.csv | tail -n 1`
     # Set Variables for the Filenames including the "barcode"
      R1=""$i""_$barcode"_"$lane"_R1_001.fastq.gz";
      R2=""$i"_"$barcode"_"$lane"_R2_001.fastq.gz";
     #Set variables for the directories
      Dir1="${prefix}/$i/${lane}/Reads/Trimmed" 
      Dir3="${prefix}/$i/${lane}/Reads/Trimmed_3'" 
      Dir5="${prefix}/$i/${lane}/Reads/Trimmed_5'" 

     #make a new target directory for the Trim-files 
     # mkdir -p "$Dir1" 

     #execute cutadapt for all the trimmed files with TrueSeq adaptors and store it to Trimmed-folder
     # echo "start Cutadapt for TrueSeq adaptors "$i" "$lane" "

     # /home/mrtnfries/anaconda3/envs/Cut/bin/cutadapt -a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -A GATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT -o "$Dir1"/trimmed."$R1" -p "$Dir1"/trimmed."$R2" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R1" "$prefix"/"$i"/"$lane"/Reads/Raw/"$R2" > "$Dir1"/logfile1
     
     # echo "Cutadapt for TrueSeq adaptors finished "$i" "$lane" "

     #make a new target directory for the Trimmed2-files
      mkdir -p "$Dir3" "$Dir5" 

    
     #execute AdapterRemoval for checking which adaptor is used?
     # echo "start Cutadapt for costomized WGA4 adaptors from 3' "$i" "$lane" "
    /home/mrtnfries/anaconda3/envs/adaRem/bin/AdapterRemoval --identify-adapters --file1 "$Dir1"/trimmed."$R1" --file2 "$Dir1"/trimmed."$R2" > "$Dir1"/remainingadapters
     # echo "---First read from 3'---"
     # grep "Sequence: TACACGACGCTCTTCCGATCT"$barcode"TGTGTTGGGTGTGTTTGG;" "$Dir3"/logfile2
     # echo "---Second read from 3'---"  
     # grep "Sequence: AGACGTGTGCTCTTCCGATCT"$barcode"TGTGTTGGGTGTGTTTGG;" "$Dir3"/logfile2
     # echo "Cutadapt for costomized WGA4 adaptors finished "$i" "$lane" "



   done

done

