

#!/bin/bash

# show the fast.qc files in every directory

prefix=~/Project/chirp/ueberham
for i in `ls $prefix`	 
do	

  for lane in L007 L008
    do
     # Set the variable "barcode": Extracting the barcode sequence out of the SampleSheet file. 
      barcode=`awk -F ',' '{print $5}'  $prefix/$i/SampleSheet.csv | tail -n 1`
     # Set Variables for the Filenames including the "barcode"
      R1="${prefix}/${i}/${lane}/Reads/Raw/${i}_${barcode}_${lane}_R1_001.fastq.gz";
      R2="${prefix}/${i}/${lane}/Reads/Raw/${i}_${barcode}_${lane}_R2_001.fastq.gz";
     
          
     #make a new target directory for the QC-files
     mkdir -p ${prefix}/$i/${lane}/QC/Raw/ 

     #execute fastqc for all the files 
     /home/mrtnfries/anaconda3/envs/qualityControl/bin/fastqc -o ${i}/${lane}/QC/Raw --noextract $R1 $R2

   done

done
